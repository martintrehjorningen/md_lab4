package mth.mobile.ntnu.com.md_lab4;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static mth.mobile.ntnu.com.md_lab4.A2_main.PREFS_USERNAME;
import static mth.mobile.ntnu.com.md_lab4.A2_main.appInForeground;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab1FragmentChat extends Fragment {
    private View mView;
    private Button tab1_button_sendMessage;
    private EditText tab1_editText_message;
    private ListView tab1_listView_messages;
    private SharedPreferences prefs;
    private List<String> allMessages = new ArrayList<>();
    //Variables for chatmessage written by user
    private String username;
    private String date;
    private String message;

    //Firebase setup
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.tab1_fragment_chat, container, false);
        prefs = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        loadPreferences();

        //Find GUI-elements
        tab1_editText_message = (EditText) mView.findViewById(R.id.tab1_EditText1_message);
        tab1_button_sendMessage = (Button) mView.findViewById(R.id.tab1_button1_send);
        tab1_listView_messages = (ListView) mView.findViewById(R.id.tab1_listView_messages);

        //Get all messages for chatfeed
        dbGetAllMessages();

        //Set up send button with listener
        tab1_button_sendMessage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View mView) {
                Log.d("tab1 button1", String.valueOf(tab1_editText_message.getText()));
                //If message is not empty
                if (!tab1_editText_message.getText().toString().equals("")) {
                    sendMessage();
                }
            }
        });

        return mView;
    }

    private void dbGetAllMessages() {
        db.collection("messages").orderBy("d", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("dbGetChatMessagesForUser", "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    //Add message to list of messages in format [username(date): message]
                                    String ingoingDate = dc.getDocument().getString("d");
                                    String ingoingMessage = dc.getDocument().getString("m");
                                    String ingoingUsername = dc.getDocument().getString("u");
                                    allMessages.add(ingoingUsername + "(" + ingoingDate + "): " + ingoingMessage);
                                    //If app is in background, create a notification
                                    if (!appInForeground) {
                                        makeNotification(ingoingUsername, ingoingMessage);
                                    } else {
                                        updateGUI();
                                    }
                                    break;
                            }
                        }

                    }
                });
    }

    @Override
    public void onResume() {
        Log.d("resume", "resume in tab1fragment");
        updateGUI();
        super.onResume();
    }

    private void updateGUI() {
        if (getActivity() != null) {
            ListAdapter listAdapter = new ArrayAdapter<>(this.getActivity(), android.R.layout.simple_list_item_1, allMessages);
            tab1_listView_messages.setAdapter(listAdapter);
        } else {
            Log.d("updateGUI", "Activity is null");
        }
    }

    private void sendMessage() {
        //Prepare a date, get the text message from the input, and prepare an object to send to db
        date = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        message = String.valueOf(tab1_editText_message.getText());
        ChatMessage newChatMessage = new ChatMessage(message, date, username);
        Log.d("sendMessage", "new message: " + newChatMessage.toString());

        //Sends the ChatMessage object to the database
        db.collection("messages")
                .add(newChatMessage)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("sendMessage", "DocumentSnapshot written with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("sendMessage", "Error adding document", e);
                    }
                });
        //Finally remove the text for the inputfield
        tab1_editText_message.setText("");
    }

    private void loadPreferences() {
        username = prefs.getString(PREFS_USERNAME, "");
        A2_main.usernameSet = (!Objects.equals(username, ""));
        Log.d("loadPreferences", "username: " + username);
        Log.d("loadPreferences", "usernameSet:  " + A2_main.usernameSet);
    }

    private void makeNotification(String ingoingUsername, String ingoingNessage) {
        Log.d("makeNotification", "Making notification");
        int ncode = 101;
        String channelId = "test_channel_id";
        String channelName = "test_channel_name";
        String channelDescription = "test_channel_description";
        Intent nIntent = new Intent(Config.context, A2_main.class);
        nIntent.setFlags(
                  Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pIntent = PendingIntent.getActivity(Config.context, 0, nIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Set up notification manager
        NotificationManager mNotific = (NotificationManager) Config.context.getSystemService(Context.NOTIFICATION_SERVICE);

        //*** Notification Channel ***
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_LOW);
            mChannel.setDescription(channelDescription);
            mChannel.canShowBadge();
            mChannel.setShowBadge(true);
            mNotific.createNotificationChannel(mChannel);
        }

        //*** Notification Builder ***
        Notification n = new Notification.Builder(Config.context, channelId)
                .setContentTitle("message from "+ingoingUsername)
                .setContentText(ingoingNessage)
                .setBadgeIconType(R.drawable.ic_chat)
                .setNumber(5)
                .setContentIntent(pIntent)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setAutoCancel(true)
                .build();
        n.flags |= Notification.FLAG_AUTO_CANCEL;

        //*** Notification Notify User ***
        mNotific.notify(ncode, n);
    }
}
