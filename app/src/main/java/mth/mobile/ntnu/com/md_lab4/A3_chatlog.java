package mth.mobile.ntnu.com.md_lab4;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class A3_chatlog extends AppCompatActivity {
    private ListView A3_listView_chatlog;
    private List<String> allMessages = new ArrayList<>();
    private String parameterUsername;
    private FirebaseFirestore db;

    /**
     * Taken directly from https://stackoverflow.com/a/31493894
     */
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            super.onBackPressed(); //replaced
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3_chatlog);

        //Get parameter username and update title
        Bundle b = getIntent().getExtras();
        if (b != null) {
            parameterUsername = b.getString("username");
            setTitle("Chatlog for " + parameterUsername);
        }

        //Get GUI-elements
        A3_listView_chatlog = findViewById(R.id.A3_listView_chatlog);

        //Fetch data from Firestore and get messages
        db = FirebaseFirestore.getInstance();
        dbGetChatMessagesForUser(parameterUsername);

    }

    private void dbGetChatMessagesForUser(String parameterUsername) {
        db.collection("messages").orderBy("d", Query.Direction.ASCENDING)
                .whereEqualTo("u", parameterUsername)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot snapshots,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("dbGetChatMessagesForUser", "listen:error", e);
                            return;
                        }

                        for (DocumentChange dc : snapshots.getDocumentChanges()) {
                            switch (dc.getType()) {
                                case ADDED:
                                    String date = dc.getDocument().getString("d");
                                    String message = dc.getDocument().getString("m");
                                    allMessages.add(date + ": "+message);
                                    Log.d("dbGetChatMessagesForUser", "New message: " + date+": "+message);
                                    updateGUI();
                                    break;
                            }
                        }

                    }
                });
    }

    private void updateGUI() {
        ListAdapter listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, allMessages);
        A3_listView_chatlog.setAdapter(listAdapter);
    }
}
