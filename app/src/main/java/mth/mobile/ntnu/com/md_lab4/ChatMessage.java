package mth.mobile.ntnu.com.md_lab4;

public class ChatMessage {
    private String m;
    private String d;
    private String u;

    public ChatMessage(String m, String d, String u) {
        this.m = m;
        this.d = d;
        this.u = u;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }

    public String getU() {
        return u;
    }

    public void setU(String u) {
        this.u = u;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "m='" + m + '\'' +
                ", d='" + d + '\'' +
                ", u='" + u + '\'' +
                '}';
    }
}
