package mth.mobile.ntnu.com.md_lab4;

/**
 * Class that only exists to provide the data structure matching my database
 */
public class User {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User(String username) {
        this.username = username;
    }
}
