package mth.mobile.ntnu.com.md_lab4;

/**
 * Class based on the following stack overflow answer: https://stackoverflow.com/a/9765146
 *
 * Its purpose is to provide access to A2_mains context from within the fragments, when the activity
 * is in the background. I was not able to access the context when attempting to create a NotificationManager
 * object through the following method: "getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);"
 *
 * As a workaround, I was able to get it to work by creating the desired object by using the following
 * method: "Config.context.getSystemService(Context.NOTIFICATION_SERVICE);"
 */
public final class Config {
    public static A2_main context = null;
}