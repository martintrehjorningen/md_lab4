package mth.mobile.ntnu.com.md_lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Random;

public class A1_login extends AppCompatActivity {

    private TextInputEditText A1_textInputEditText_Username;
    private Button A1_button_StartChatting;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String usernameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a1_login);

        //Capture and setup GUI-elements
        A1_textInputEditText_Username = findViewById(R.id.A1_textInputEditText_Username);
        A1_button_StartChatting = findViewById(R.id.A1_button_StartChatting);

        //Set up listener to register username in database and in sharedpreferences upon buttonclick
        A1_button_StartChatting.setOnClickListener(new AdapterView.OnClickListener() {
            @Override
            public void onClick(View v) {
                usernameInput = A1_textInputEditText_Username.getText().toString();
                //User the username field is not empty, return the value to A2_main
                if (!usernameInput.equals("")) {
                    Log.d("onClick i = R.id.A1_button_StartChatting", "Setter username til " + usernameInput);
                    dbRegisterUsername();
                    Intent intent = new Intent();
                    intent.putExtra("username", usernameInput);
                    setResult(RESULT_OK, intent);
                    finish();
                }
                //If username field is empty, a message pops up to remind the user to fill it in
                else {
                    Toast.makeText(A1_login.this, "Please fill in username",
                            Toast.LENGTH_LONG).show();
                }
            }
        });

        //Set suggested username to be a random 9 digit number
        String suggestedUsername = String.valueOf(Math.floor(Math.random() * 1000000000));
        A1_textInputEditText_Username.setText(suggestedUsername);
    }

    /**
     * Exits the application when back key is pressed. This forces the user to fill in a username
     * before proceeding to use the chat
     */
    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    private void dbRegisterUsername() {
        //Create User object holding username
        User newUser = new User(usernameInput);

        //Insert into users collection in db
        db.collection("users")
                .add(newUser)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("sendMessage", "DocumentSnapshot written with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("sendMessage", "Error adding document", e);
                    }
                });
    }
}



