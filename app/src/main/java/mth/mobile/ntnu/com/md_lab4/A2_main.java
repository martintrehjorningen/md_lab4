package mth.mobile.ntnu.com.md_lab4;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Objects;

public class A2_main extends AppCompatActivity {
    public static final int A1_LOGIN_USERNAME_CODE = 1;
    public static final String PREFS_USERNAME = "username";
    public static String PACKAGE_NAME;
    public static boolean appInForeground = false;
    public static boolean usernameSet = false;
    public static String username;
    private TabLayout tabLayout;
    private SharedPreferences prefs;
    private MyPagerAdapter myPagerAdapter;
    private ViewPager viewPager;
    private Tab1FragmentChat fragmentChat;
    private Tab2FragmentFriends fragmentFriends;
    //Icons for tabs
    private int[] tabIcons = {
            R.drawable.ic_chat,
            R.drawable.ic_friends
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2_main);
        PACKAGE_NAME = getApplicationContext().getPackageName();
        Config.context = this;

        //Get data from sharedpreferences and checks login
        prefs = getPreferences(MODE_PRIVATE);
        loadPreferences();
        enforceUsername();

        //Set up adapter and viewpager
        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.A2_container);
        setupViewPager(viewPager);

        //Set up tablayout
        tabLayout = (TabLayout) findViewById(R.id.A2_tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        Objects.requireNonNull(tabLayout.getTabAt(0)).setIcon(tabIcons[0]);
        Objects.requireNonNull(tabLayout.getTabAt(1)).setIcon(tabIcons[1]);
        Tab1FragmentChat fragment = (Tab1FragmentChat) getSupportFragmentManager().findFragmentByTag("tab1_fragment_chat");
    }

    @Override
    protected void onStart() {
        super.onStart();
        appInForeground = true;
    }

    @Override
    public void onResume() {
        super.onResume();
        appInForeground = true;
        Log.d("onResume", "app now in foreground");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("onPause", "app now in background");
        appInForeground  = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Sets the username to returned key value, and updates sharedprefs
        if (requestCode == A1_LOGIN_USERNAME_CODE) {
            if(resultCode == RESULT_OK) {
                username = data.getStringExtra("username");
                SaveUsernameToPreferences();
                Log.d("onActivityResult A1_LOGIN_USERNAME_CODE", "Setting username to "+username);
            }
        }
    }

    private void enforceUsername() {
        if (!usernameSet) {
            Log.d("enforceUsername", "username not set, prompting user for input");
            launchActivityA1_login();
        }
        else {
            Log.d("enforceUsername", "username already set: "+username);
        }
    }

    /**
     * launches the A1_login activity to get a username from the user
     */
    private void launchActivityA1_login() {
        Log.d("launchActivityA1_login", "Launching");
        Intent i = new Intent(this, A1_login.class);
        startActivityForResult(i, A1_LOGIN_USERNAME_CODE);
    }

    private void setupViewPager(ViewPager viewPager) {
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        adapter.addNewFragment(new Tab1FragmentChat(), "chat");
        adapter.addNewFragment(new Tab2FragmentFriends(), "friends");
        viewPager.setAdapter(adapter);
    }

    private void loadPreferences() {
        username = prefs.getString(PREFS_USERNAME, "");
        usernameSet = (!Objects.equals(username, ""));
        Log.d("loadPreferences", "username: "+username);
        Log.d("loadPreferences", "usernameSet:  "+usernameSet);
    }

    private void SaveUsernameToPreferences() {
        Log.d("SaveUsernameToPreferences", "Saving username to preferences: "+username);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(PREFS_USERNAME, username);
        edit.apply();
    }
}
