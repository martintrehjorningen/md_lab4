package mth.mobile.ntnu.com.md_lab4;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static mth.mobile.ntnu.com.md_lab4.A2_main.appInForeground;

/**
 * A simple {@link Fragment} subclass.
 */
public class Tab2FragmentFriends extends Fragment {
    private List<String> allUsers = new ArrayList<>();
    HashSet<String> hashSet = new HashSet<>();
    private ListView tab2_listView_friends;
    private FirebaseFirestore db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.tab2_fragment_friends, container, false);

        //Get gui-elements
        tab2_listView_friends = mView.findViewById(R.id.tab2_listView_friends);

        //Fetch data from Firestore
        db = FirebaseFirestore.getInstance();
        dbGetAllUsers();

        //Set up listener to open chatlog of any user clicked in the friendslist
        tab2_listView_friends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("onItemClick", "user: "+allUsers.get(position));

                Intent intent = new Intent(getContext(), A3_chatlog.class);
                intent.putExtra("username", allUsers.get(position));
                startActivity(intent);
            }
        });
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        updateGUI();
    }

    /**
     * Fetches users alphabetically and adds them to a hashset to guarantee distinct users.
     * Then adds all users from the hashset to the userlist, and updates GUI.
     */
    private void dbGetAllUsers() {
        db.collection("users").orderBy("username", Query.Direction.ASCENDING)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value,
                                        @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            Log.w("dbGetAllUsers", "Listen failed.", e);
                            return;
                        }
                        List<String> newSetOfUsers = new ArrayList<>();
                        for (QueryDocumentSnapshot doc : value) {
                            if (doc.get("username") != null) {
                                newSetOfUsers.add(doc.getString("username"));
                            }
                        }
                        hashSet.addAll(newSetOfUsers);
                        allUsers.clear();
                        allUsers.addAll(hashSet);
                        if (appInForeground) {
                            updateGUI();
                        }
                    }
                });
    }

    private void updateGUI() {
        //If the new list of users is empty, the old one does not get replaced
        if (getActivity() != null) {
            if (allUsers.size() == 0) {
                Log.d("updateGUI", "No users in allUsers list");
                return;
            }
        }
        Collections.sort(allUsers);
        ListAdapter listAdapter = new ArrayAdapter<>(this.getContext(), android.R.layout.simple_list_item_1, allUsers);
        tab2_listView_friends.setAdapter(listAdapter);
    }
}
